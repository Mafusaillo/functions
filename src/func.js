const getSum = (str1, str2) => {
  if (str1.length === 0 && str2.length === 0 || typeof(str1) === "object" || typeof(str2) === "object"){
    return false;
  }
  else if (str1.length === 0){
    return str2;
  }
  else if (str2.length === 0){
    return str1;
  }
  else if (typeof(str1) !== "string" || typeof(str2) !== "string"){
    return false;
  }
  else if(!isNaN(str1) && !isNaN(str2)){
    return (parseInt(str1) + parseInt(str2)).toString();
  }
  else{
    return false}
};

const getQuantityPostsByAuthor = (listOfPosts2, authorName) => {
  let postCounter = 0;
  let commentCounter = 0;
  for (let post of listOfPosts2){
    if(post.author === authorName){
      postCounter++
    }
  }

  for (let post of listOfPosts2){
    if(post.hasOwnProperty("comments")){
      for (let comment of post.comments){
        if (comment.author === authorName){
          commentCounter++;
        }
      }
    }
  }

  return "Post:" + postCounter + ",comments:" + commentCounter;
};

const tickets=(people)=> {
  let maxEl = people[0];
  for(let human of people){
    if (human === 25 || human === 50 || human === 100){
      if (human > maxEl){
        maxEl = human;
      }
    }
    else {
      return "NO";
    }
  }
  let newArr = [];
  for(let human of people){
    if (human !== maxEl){
      newArr.push(human);
    }
  }

  const sum = newArr.reduce((partialSum, a) => partialSum + a, 0);
  if (sum >= maxEl){
    return "YES"
  }
  else{
    return "NO";
  }
}



module.exports = {getSum, getQuantityPostsByAuthor, tickets};


//